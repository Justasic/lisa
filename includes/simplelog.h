/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once
#include <mutex>
#include <string_view>
#include <thread>
#include <utility>
#include <algorithm>
#include <ctime>
#include "fmt/format.h"
#include "fmt/color.h"
#include "fmt/std.h"
#include "fmt/chrono.h"

extern std::mutex ptx;

template<typename T>
void common_log_no_newline(T &&type, std::string_view message)
{
	std::lock_guard<std::mutex> lock(ptx);
	time_t t = std::time(nullptr);

	fmt::print("[{:>8}] [T{}] [{:%Y-%m-%d %T}]: {}\r", std::move(type), std::this_thread::get_id(), fmt::localtime(t), std::string(message));
}

template<typename T>
void common_log(T &&type, std::string_view message)
{
	std::lock_guard<std::mutex> lock(ptx);
	time_t t = std::time(nullptr);

	fmt::print("[{:>8}] [T{}] [{:%Y-%m-%d %T}]: {}\n", std::move(type), std::this_thread::get_id(), fmt::localtime(t), std::string(message));
}

template<typename... args>
void ilog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log(fmt::styled("INFO", fmt::fg(fmt::color::green)), std::move(formatted));
}

template<typename... args>
void wlog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log(fmt::styled("WARNING", fmt::fg(fmt::color::orange)), std::move(formatted));
}

template<typename... args>
void elog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log(fmt::styled("ERROR", fmt::fg(fmt::color::red)), std::move(formatted));
}

template<typename... args>
void clog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log(fmt::styled("CRITICAL", fmt::fg(fmt::color::red) | fmt::emphasis::underline), std::move(formatted));
}

// No newline variants.
template<typename... args>
void icrlog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log_no_newline(fmt::styled("INFO", fmt::fg(fmt::color::green)), std::move(formatted));
}

template<typename... args>
void wcrlog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log_no_newline(fmt::styled("WARNING", fmt::fg(fmt::color::orange)), std::move(formatted));
}

template<typename... args>
void ecrlog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log_no_newline(fmt::styled("ERROR", fmt::fg(fmt::color::red)), std::move(formatted));
}

template<typename... args>
void ccrlog(std::string_view format, args &&...a)
{
	auto formatted = fmt::format(fmt::runtime(format), a...);
	common_log_no_newline(fmt::styled("CRITICAL", fmt::fg(fmt::color::red) | fmt::emphasis::underline), std::move(formatted));
}
