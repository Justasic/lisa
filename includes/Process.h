/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once
#include <sys/epoll.h>
#include <string>
#include <vector>
#include <utility>
#include <unordered_map>
#include <memory>
#include "Socket.h"

class ProcessPipe : public Pipe 
{
public:
	std::string buffer;

	ProcessPipe() = default;
	~ProcessPipe() = default;
	constexpr ProcessPipe(ProcessPipe &&rhs) : Pipe(std::move(rhs)) {}
	constexpr ProcessPipe &operator=(ProcessPipe &&rhs) noexcept
	{
		if (&rhs == this) [[unlikely]]
			return *this;

		return static_cast<ProcessPipe&>(Pipe::operator=(std::move(rhs)));
	}


	bool MultiplexRead() override;

	// Ability to close one side of the pipe.
	// this saves on open FDs.
	void CloseRead() noexcept;
	void CloseWrite() noexcept;
};

class BasicProcess 
{
protected:
	// void ClosePipes(pipepair &pair);
public:
	pid_t process_id_;
	// Each of these handles reading and writing to/from stdin/out/err
	// the 0th element is our FD, the 1st element is the child's
	ProcessPipe chld_stdin_;
	ProcessPipe chld_stdout_;
	ProcessPipe chld_stderr_;

	// Exit status of the child process
	int exit_code_;
	
	// Whether the process has exited or not
	bool exited_;
	
	// buffers
	mutable std::string stdout_;
	mutable std::string stderr_;

	// Time since the last message edit
	// time_t lastedit;
	
	BasicProcess();	
	// This class is not copyable
	BasicProcess(const BasicProcess &) = delete;
	BasicProcess &operator=(const BasicProcess &) = delete;
	
	// Define move constructor
	BasicProcess(BasicProcess &&rhs) noexcept; 	

	// Define move operator
	constexpr BasicProcess &operator=(BasicProcess &&rhs) noexcept
	{
		if (&rhs == this) [[unlikely]]
			return *this;
		
		this->process_id_ = std::move(rhs.process_id_);
		// this->chld_stdin_ = std::move(rhs.chld_stdin_);
		// this->chld_stdout_ = std::move(rhs.chld_stdout_);
		// this->chld_stderr_ = std::move(rhs.chld_stderr_);
		this->exit_code_ = std::move(rhs.exit_code_);
		this->exited_ = std::move(rhs.exited_);
		this->stdout_ = std::move(rhs.stdout_);
		this->stderr_ = std::move(rhs.stderr_);
		
		return *this;
	}
	
	// When the class is destructed, close resources and
	// terminate the process (if running)
	virtual ~BasicProcess();

	virtual bool ExecuteProcess(std::vector<std::string> &&arguments, std::string &&stdin = {}) = 0;
};

class ProcessManager
{
	static ProcessManager *me;

	friend class BasicProcess;
	
	static void HandleSIGCHLD(int signal);
	void ProcessChildState(pid_t pid, int wstatus);
public:

	// Association of running processed whose parent is us.
	std::unordered_map<pid_t, std::unique_ptr<BasicProcess>> processes;
	
	ProcessManager();

	// Karen's favorite function
	static ProcessManager *GetManager() { return ProcessManager::me; }
	
	bool Initialize();
	void Terminate();
	
	void ProcessChildren();

	void CheckChildren();
};
