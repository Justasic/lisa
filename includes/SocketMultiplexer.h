/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once
#include "Socket.h"
#include <list>
#include <vector>
#include <sys/epoll.h>

typedef struct epoll_event epoll_t;

class SocketMultiplexer
{
  protected:
	// Used for finding sockets as well as handling other things
	// like initialization of sockets.
	std::list<Socket *> Sockets;

	// Specific to epoll
	int                  epollhandle;
	std::vector<epoll_t> Events;

	// for the getter
	static SocketMultiplexer *mplex_;

  public:
	SocketMultiplexer();

	static SocketMultiplexer *GetMultiplexer();

	virtual ~SocketMultiplexer();

	// Initalizers
	virtual int Initialize();
	virtual void Terminate();

	// Sockets interact with these functions.
	virtual bool AddSocket(Socket *s);
	virtual bool RemoveSocket(Socket *s);
	Socket *	 FindSocket(int sock_fd);
	virtual bool UpdateSocket(Socket *s);

	// This is called in the event loop to slow the program down and process sockets.
	virtual void Multiplex(time_t sleep);
};
