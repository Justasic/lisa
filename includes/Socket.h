/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once
#include <arpa/inet.h>
#include <atomic>
#include <netinet/in.h>
#include <string_view>
#include <sys/socket.h>
#include <sys/un.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string>

template<typename T, typename INTERN = uint32_t>
class Flags
{
  protected:
	std::atomic<INTERN> Flag_Values;

  public:
	/** Add a flag to this item
	 * @param Value The flag
	 */
	void SetFlag(T Value)
	{
		this->Flag_Values |= (static_cast<INTERN>(1) << Value);
	}

	/**
	 * Add a set of flags to this item
	 * @method SetFlags
	 * @param  value    the first flag to add
	 * @param  values   the remaining flags to add
	 */
	template<typename... Args>
	void SetFlags(Args... values)
	{
		// C++ 17 fold expression
		(this->Flag_Values.fetch_or((static_cast<INTERN>(1) << values)), ...);
	}

	/** Remove a flag from this item
	 * @param Value The flag
	 */
	void RemoveFlag(T Value)
	{
		this->Flag_Values &= ~(static_cast<INTERN>(1) << Value);
	}

	/**
	 * Remove a set of flags from this item
	 * @method RemoveFlags
	 * @param  value    the first flag to Remove
	 * @param  values   the remaining flags to Remove
	 */
	template<typename... Args>
	void RemoveFlags(Args... values)
	{
		// C++ 17 fold expression
		(this->Flag_Values.fetch_and(~(static_cast<INTERN>(1) << values)), ...);
	}

	/** Check if this item has a flag
	 * @param Value The flag
	 * @return true or false
	 */
	bool HasFlag(T Value) const { return static_cast<bool>((Flag_Values.load() >> Value) & 0x01); }

	/** Check how many flags are set
	 * @return The number of flags set
	 */
	size_t FlagCount() const { return __builtin_popcount(this->Flag_Values.load()); }

	/** Unset all of the flags
	 */
	void ClearFlags() { Flag_Values.store(0); }
};

// clang-format off
typedef enum
{
	// Misc socket statuses.
	SS_DEAD       ,//= 1 << 1,
	SS_WRITABLE   ,//= 1 << 2,
	// For connecting sockets
	SS_CONNECTING ,//= 1 << 3,
	SS_CONNECTED  ,//= 1 << 4,
	// For SSL sockets.
	SS_SSL_HANDSHAKING,
	// For Binding sockets.
	SS_ACCEPTING  ,//= 1 << 5,
	SS_ACCEPTED   ,//= 1 << 6,
	// Multiplexer statuses
	MX_WRITABLE   ,//= 1 << 7,
	MX_READABLE   ,//= 1 << 8
} socketstatus_t;
// clang-format on

union SocketAddress {
	struct sockaddr sa;
	struct sockaddr_in v4;
	struct sockaddr_in6 v6;
	struct sockaddr_un unx;
};

struct SocketError 
{
	std::string message;
	int code;
};

class Socket : public Flags<socketstatus_t>
{
  protected:
	int        sock_fd;
	SocketAddress sa;

	SocketError se;

	inline void SetError(int code, std::string message = "")
	{
		this->se.code = code;
		if (message.empty())
			this->se.message = strerror(code);
	}

  public:
	// Delete the default constructor because it causes all kinds of fucking issues.
	Socket() = delete;
	Socket(const Socket&) = delete;
	constexpr Socket(Socket &&rhs) noexcept
	{
		this->sock_fd = rhs.sock_fd;
		this->sa = std::move(rhs.sa);
		this->se = std::move(rhs.se);

		// Reset stuff
		rhs.sock_fd = -1;
	}

	Socket &operator=(const Socket&) = delete;
	constexpr Socket &operator=(Socket &&rhs)
	{
		if (&rhs == this) [[unlikely]]
			return *this;

		this->sock_fd = rhs.sock_fd;
		this->sa = std::move(rhs.sa);
		this->se = std::move(rhs.se);

		// Reset stuff
		rhs.sock_fd = -1;

		return *this;
	}


	Socket(int sock, int type = AF_INET, int protocol = SOCK_STREAM);
	virtual ~Socket();

	// Socket Flags
	void                SetNonBlocking(bool status);

	// Socket Error state, used to avoid exceptions.
	inline bool HasError() { return se.code != 0 && !se.message.empty(); }
	inline SocketError GetErrorInfo() { return this->se; }

	// I/O functions, these are overwritten in the sub-class sockets.
	virtual ssize_t     Write(const void *data, size_t len);
	virtual ssize_t     Read(void *data, size_t len);

	// Getters/Setters
	inline int          GetFD() { return this->sock_fd; }

	// Interaction with the SocketMultiplexer
	virtual bool        MultiplexEvent();
	virtual void        MultiplexError();
	virtual bool        MultiplexRead();
	virtual bool        MultiplexWrite();
};

class ConnectionSocket : public Socket
{
	std::string address;
	in_port_t        port;

  public:
	ConnectionSocket(int sock, int type = AF_INET, int protocol = SOCK_STREAM);
	virtual ~ConnectionSocket();

	bool                MultiplexEvent();
	void                MultiplexError();

	void                Connect(const std::string &address, in_port_t port);

	virtual void        OnConnect() = 0;
	virtual void        OnError(const std::string &str);

	inline std::string GetAddress() const { return this->address; }
	inline in_port_t    GetPort() const { return this->port; }
};

class Pipe : public Socket
{
public:
	/** Move constructors necessary
	 * for transfering stuff.
	 */
	constexpr Pipe(Pipe &&rhs) noexcept : Socket(std::move(rhs))
	{
		this->write_pipe = rhs.write_pipe;
		rhs.write_pipe = -1;
	}
	constexpr Pipe &operator=(Pipe &&rhs)
	{
		if (&rhs == this) [[unlikely]]
			return *this;

		this->write_pipe = rhs.write_pipe;

		// Reset stuff
		rhs.write_pipe = -1;

		return *this;
	}

	/** The FD of the write pipe
	 * this->sock is the readfd
	 */
	int write_pipe;

	Pipe();
	~Pipe();

	/** Write data to this pipe
	 * @param data The data to write
	 * @param sz The amount of data to wirite
	 */
	// void Write(const char *data, size_t sz);
	inline void Write(const std::string &data) { this->Write(data.c_str(), data.length() + 1); }
	virtual ssize_t Write(const void *data, size_t len) override;
	

	/** Read data from this pipe
	 * @param data A buffer to read data into
	 * @param sz The size of the buffer
	 * @return The amount of data read
	 */
	virtual ssize_t Read(void *data, size_t len) override;
	// int Read(char *data, size_t sz);

	/** Mark the write end of this pipe (non)blocking
	 * @param state true to enable blocking, false to disable blocking
	 * @return true if the socket is now blocking
	 */
	bool SetWriteBlocking(bool state);
};

class SecureConnectionSocket : public Socket
{
	SSL_CTX *ctx;
	SSL 	*ssl;
	std::string address;
	in_port_t	 port;

	inline std::string get_ssl_errors()
	{
		unsigned long int err = 0;
		std::string errbuf;
		while ((err = ERR_get_error()))
		{
			errbuf += ERR_error_string(err, nullptr);
			errbuf += '\n';
		}
		return errbuf;
	}

	void InitializeSSL();

  public:
	bool MultiplexEvent() override;
	void MultiplexError() override;

	void		Connect(const std::string &address, in_port_t port);
	inline SSL *GetSSL() const { return this->ssl; }

	virtual void OnError(const std::string &str);

	SecureConnectionSocket(int sock, int type = AF_INET, int protocol = SOCK_STREAM);
	virtual ~SecureConnectionSocket();

	// Because OpenSSL requires we use their custom functions.
	virtual ssize_t Write(const void *data, size_t len) override;
	virtual ssize_t Read(void *data, size_t len) override;

	// Override this so when the connection is successful we can start SSL.
	virtual void OnConnect() = 0;
};

class ClientSocket;

class ListeningSocket : public Socket
{
  protected:
	std::string address;
	in_port_t    port;
	bool         ipv6;

  public:
	ListeningSocket(const std::string &bindaddr, in_port_t port, int domain);
	virtual ~ListeningSocket();
	bool                  MultiplexRead();

	virtual ClientSocket *OnAccept(int fd, SocketAddress &&addr) = 0;
};

class ClientSocket : public Socket
{
  public:
	ListeningSocket *ls;
	ClientSocket(ListeningSocket *ls, int sock_fd, SocketAddress &&addr);
	bool         MultiplexEvent();
	void         MultiplexError();
	virtual void OnAccept();
	virtual void OnError(const std::string &str);
};
