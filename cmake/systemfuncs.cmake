#
# Copyright (c) 2024, Justin Crawford
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


###############################################################################
# GetSources(<source directory> <output C sources> <output C++ sources>)
#
# A macro to recursively get source files, however if the source folder has
# a CMakeLists.txt file, include it as part of the build instead.
###############################################################################
macro(GetSources SRC CSOURCES CPPSOURCES)
	if(NOT ${SRC} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR} AND EXISTS "${SRC}/CMakeLists.txt")
		# If we have a CMakeLists.txt file, include it instead of trying to compile
		# sources as part of main project.
		add_subdirectory("${SRC}")
		# Allow the child CMakeLists.txt to add sources to our list.
		list(APPEND ${CPPSOURCES} ${CHILD_SCOPE_SOURCES})
	else(NOT ${SRC} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR} AND EXISTS "${SRC}/CMakeLists.txt")
		# get a list of all files in the directory.
		file(GLOB SRC_FLDR "${SRC}/*")
		foreach(SOURCEFILE ${SRC_FLDR})
			# If it's a directory, recursively call this function.
			if (IS_DIRECTORY "${SOURCEFILE}")
				GetSources(${SOURCEFILE} ${CSOURCES} ${CPPSOURCES})
			else (IS_DIRECTORY "${SOURCEFILE}")
				# Otherwise look for source files and append them.
				# Look for C++ files.
				string(REGEX MATCH "\\.cpp$" CPP ${SOURCEFILE})
				if (CPP)
					list(APPEND ${CPPSOURCES} ${SOURCEFILE})
				endif (CPP)

				# Look for C files.
				string(REGEX MATCH "\\.c$" C ${SOURCEFILE})
				if (C)
					list(APPEND ${CSOURCES} ${SOURCEFILE})
				endif (C)
			endif (IS_DIRECTORY "${SOURCEFILE}")
		endforeach(SOURCEFILE ${SRC_FLDR})
	endif(NOT ${SRC} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR} AND EXISTS "${SRC}/CMakeLists.txt")
endmacro(GetSources)

function(GetGitRevision)
# Get the git revision location for the branch we're on
	if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git/HEAD")
		file(READ ${CMAKE_CURRENT_SOURCE_DIR}/.git/HEAD GIT_HEAD_LOC)
		string(LENGTH ${GIT_HEAD_LOC} HEAD_LEN)
		math(EXPR LEN "${HEAD_LEN} - 5")
		string(SUBSTRING ${GIT_HEAD_LOC} 5 ${LEN} GIT_HEAD)
		# Weird nastery to remove newlines which screw up the if statement below.
		set(GIT_SHA_PATH "${CMAKE_CURRENT_SOURCE_DIR}/.git/${GIT_HEAD}")
		string(REGEX REPLACE "(\r?\n)+$" "" GIT_SHA_PATH "${GIT_SHA_PATH}")
	endif(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git/HEAD")

# Get the git revision we're on for the version string
	if(EXISTS "${GIT_SHA_PATH}")
		file(READ "${GIT_SHA_PATH}" VERSION_STR)
		string(REGEX REPLACE "(\r?\n)+$" "" VERSION_STR "${VERSION_STR}")
		# Get the length of the string
		string(LENGTH ${VERSION_STR} VERSION_LEN)
		# Subtract 7 from the string's length
		math(EXPR VERSION_NUM_LEN "${VERSION_LEN} - ${VERSION_LEN} + 7")
		# Extract the value from the string
		string(SUBSTRING ${VERSION_STR} 0 ${VERSION_NUM_LEN} VERSION_GIT)
	endif(EXISTS "${GIT_SHA_PATH}")

	# Set our variables
	set(GIT_REVISION_LONG ${VERSION_STR} PARENT_SCOPE)
	set(GIT_REVISION_SHORT ${VERSION_GIT} PARENT_SCOPE)
endfunction(GetGitRevision)
