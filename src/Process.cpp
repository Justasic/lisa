/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "simplelog.h"
#include "Process.h"
#include <iostream>
#include <sys/epoll.h>
#include <sys/wait.h>

ProcessManager *ProcessManager::me = nullptr;

void ProcessManager::ProcessChildState(pid_t pid, int wstatus)
{
	// find the child
	auto it = this->processes.find(pid);
	if (it == this->processes.end())
		return;
	
	std::unique_ptr<BasicProcess> &child = it->second;
	
	ilog("[Process] Child received a signal? {}", WIFSIGNALED(wstatus) ? "yes" : "no");
	ilog("[Process] Child Exited normally? {}", WIFEXITED(wstatus) ? "yes" : "no");
	
	// If the child crashed, print something useful.
	if (WIFSIGNALED(wstatus) || WIFEXITED(wstatus))
	{
		if (WIFSIGNALED(wstatus))
			child->exit_code_ = WTERMSIG(wstatus);
		else
			child->exit_code_ = WEXITSTATUS(wstatus);
		
		child->exited_ = true;
		
		ilog("[Process] Child exited with code {}", child->exit_code_);
		
		// TODO: maybe dynamically update the message with running code?

		child->chld_stdout_.MultiplexRead();
		child->chld_stderr_.MultiplexRead();
		
		ilog("[Process] Finished reading child's file descriptors");
		
		// Write our reply (if applicable)
		std::string response;
		
		if (WIFSIGNALED(wstatus))
			response += fmt::format("Child received signal: {} ({})\n", child->exit_code_, strsignal(child->exit_code_));
		else
			response += fmt::format("Exit Code: {}\n", child->exit_code_);

		if (!child->stdout_.empty())
			response += child->stdout_;
		if (!child->stderr_.empty())
			response += fmt::format("stderr:\n{}", child->stderr_);


		std::cout << response << std::endl;
		
		ilog("[Process] Removing child");
		// Remove the child
		this->processes.erase(it);
	}
}

void ProcessManager::HandleSIGCHLD(int signal)
{
	// "[Process] Received signal from child: {} {}"_l(signal, strsignal(signal));
	if (signal == SIGCHLD)
	{
		int wstatus{0};
		pid_t pid = wait(&wstatus);
		
		ProcessManager *pm = ProcessManager::GetManager();
		pm->ProcessChildState(pid, wstatus);
	}
}

ProcessManager::ProcessManager()
{
	if (ProcessManager::me)
		throw std::runtime_error("ProcessManager already exists!");
	ProcessManager::me = this;
}

bool ProcessManager::Initialize()
{
	struct sigaction sa;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = &ProcessManager::HandleSIGCHLD;
	
	if (sigaction(SIGCHLD, &sa, nullptr) < 0)
		elog("[Process] Failed to set sigaction: {}", strerror(errno));
	
	return true;
}

void ProcessManager::Terminate()
{
	// TODO: terminate all subprocesses
	this->processes.clear();
	
	// Remove our signal handler.
	signal(SIGCHLD, SIG_IGN);
}

void ProcessManager::CheckChildren()
{
	// Periodically iterate the children and ensure
	// they aren't in some weird waiting state.
	for (auto &&[pid, process] : this->processes)
	{
		// Skip initializing or dead processes.
		if (pid == -1)
			continue;

		int wStatus = 0;
		pid_t retpid = waitpid(pid, &wStatus, WNOHANG);

		if (retpid != 0 && retpid != -1)
		{
			// Process has changed state
			this->ProcessChildState(pid, wStatus);
		}
		else if (retpid == -1)
		{
			// waitpid had an error.
			ilog("[Process] Error whipping child {}: {}", pid, strerror(errno));
		}
	}
}

BasicProcess::BasicProcess() : process_id_(0), exit_code_(0), exited_(false), stdout_(""), stderr_("") 
{
}

BasicProcess::BasicProcess(BasicProcess &&rhs) noexcept : process_id_(std::move(rhs.process_id_)), chld_stdin_(std::move(rhs.chld_stdin_)),
	chld_stdout_(std::move(rhs.chld_stdout_)), chld_stderr_(std::move(rhs.chld_stderr_)),
	exit_code_(std::move(rhs.exit_code_)), exited_(std::move(rhs.exited_)), stdout_(std::move(rhs.stdout_)),
	stderr_(std::move(rhs.stderr_))
{
}

BasicProcess::~BasicProcess()
{
	// We won't be able to manage this process anymore,
	// ensure it's killed.
	// FIXME: This shit killed my entire KDE user session.
	if (!this->exited_ && this->process_id_ != 0)
		ilog("OHHHH I'M BEING NAUGHTY AND KILLING THE WRONG PROCESS {}", this->process_id_);
		// kill(this->process_id, SIGKILL);
	
	// Reap the zombie process.
	ilog("[Process] Waiting for {} to exit", this->process_id_);
	int whocares;
	waitpid(this->process_id_, &whocares, 0);
}


bool BasicProcess::ExecuteProcess(std::vector<std::string> &&arguments, std::string &&stdin)
{
	// Get our process manager
	ProcessManager *pm = ProcessManager::GetManager();
	
	// Fork OwO
	this->process_id_ = fork();
	
	// !! WAIT !!
	// Before we execute, we need to update the pids.
	const auto &us = std::find_if(pm->processes.begin(), pm->processes.end(), [this](auto &it)
	{
		return it.second.get() == this;
	});
	
	// Temporarily take ownership of ourselves since there's no way to "rename"
	// a key in std::map without issues.
	std::unique_ptr<BasicProcess> proc = std::move(us->second);
	pm->processes.erase(us);
	pm->processes.emplace(this->process_id_, std::move(proc));
	
	if (this->process_id_ == -1)
	{
		wlog("[Shell] Failed to fork a child: {}", strerror(errno));
		
		return false;
	}
	else if (this->process_id_ == 0)
	{
		// We are the child, setup stdin/out/err
		while ((dup2(this->chld_stdin_.GetFD(), STDIN_FILENO) == -1) && (errno == EINTR)) {}
		while ((dup2(this->chld_stdout_.write_pipe, STDOUT_FILENO) == -1) && (errno == EINTR)) {}
		while ((dup2(this->chld_stderr_.write_pipe, STDERR_FILENO) == -1) && (errno == EINTR)) {}

		// Close all the parent's fds since they're for the parent.
		std::vector<const char *> _args;
		// Safe because the lifetime of the above vector is longer than
		// this vector.
		for (auto && it : arguments)
			_args.emplace_back(std::move(it.c_str()));
		// Last argument must be a null pointer
		_args.emplace_back(nullptr);
		// Execute the process
		execvp(_args[0], const_cast<char *const*>(_args.data()));
		for (size_t i = 0; i < arguments.size(); ++i)
			printf("argv[0]: %s\n", arguments[i].c_str());
		perror("execvp");
		// Exit, the `exit()` function deadlocks which is problematic, so here we explicitly make a syscall
		// to request our process be terminated.
		_exit(69);
	}
	else
	{
		this->chld_stdin_.CloseRead();
		this->chld_stdout_.CloseWrite();
		this->chld_stderr_.CloseWrite();

		// We are the parent, write to stdin
		if (!stdin.empty())
			this->chld_stdin_.Write(stdin);
	}
	return true;
}

void ProcessPipe::CloseRead() noexcept
{
	::close(this->GetFD());
	this->sock_fd = -1;
}

void ProcessPipe::CloseWrite() noexcept
{
	::close(this->write_pipe);
	this->write_pipe = -1;
}

bool ProcessPipe::MultiplexRead()
{
	char buf[1 << 11];
	
	ssize_t len = ::read(this->GetFD(), buf, sizeof(buf));
	if (len < 0)
	{
		wlog("[Process] Error reading from child's stdout or stderr: {}", strerror(errno));
		return false;
	}
	else if (static_cast<size_t>(len) < sizeof(buf))
	{
		// We're done reading, tell epoll such.
		ProcessManager *pm = ProcessManager::GetManager();

		// is this code below even necessary?
		
		// struct epoll_event ev;
		// ev.events = EPOLLIN;
		// ev.data.fd = is_stdout ? this->chld_stdout_.readpipe : this->chld_stderr_.readpipe;
		// 
		// if (epoll_ctl(pm->epollfd, EPOLL_CTL_MOD, ev.data.fd, &ev))
		// 	"[Process] [epoll_ctl] Failed to modify socket to epoll: {}"_lw(strerror(errno));
	}
	
	this->buffer.append(buf, buf+len);
	return true;
}
