/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Socket.h"
#include "SocketMultiplexer.h"
#include "simplelog.h"
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

Socket::Socket(int sock, int type, int protocol)
{
	memset(&this->sa, 0, sizeof(SocketAddress));
	if (sock == -1)
	{
		// https://rigtorp.se/sockets/
		this->sock_fd = ::socket(type, protocol | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);
		// We pretty much ALWAYS want non-blocking sockets for this system.
		// this->SetNonBlocking(true);
		
		SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();

		// If we have a socket engine, add our socket to it, otherwise throw an exception.
		if (!mplexer->AddSocket(this))
		{
			elog("Cannot add socket to the multiplexer: {}", strerror(errno));
			return;
		}

		this->SetFlags(MX_READABLE, MX_WRITABLE);

		if (!mplexer->UpdateSocket(this))
		{
			elog("Cannot add the socket to the multiplexer: {}", strerror(errno));
			return;
		}
	}
	else
		this->sock_fd = sock;
}

Socket::~Socket()
{
	ilog("[Socket Engine] Destroying socket {}", this->sock_fd);

	SocketMultiplexer::GetMultiplexer()->RemoveSocket(this);

	::close(this->sock_fd);
}

void Socket::SetNonBlocking(bool status)
{
	// Get the socket flags
	int flags = fcntl(this->sock_fd, F_GETFL, 0);

	if (status)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	if (fcntl(this->sock_fd, F_SETFL, flags) == -1)
		ilog("Cannot set socket {} as non-blocking with flag O_NONBLOCK: {}", this->sock_fd, strerror(errno));
}

ssize_t Socket::Write(const void *data, size_t len)
{
	if (!data || !len)
		return 0;

	return ::send(this->sock_fd, data, len, 0);
}

ssize_t Socket::Read(void *data, size_t len) { return ::recv(this->sock_fd, data, len, 0); }

bool Socket::MultiplexEvent() { return true; }
void Socket::MultiplexError() {}
bool Socket::MultiplexRead() { return true; }
bool Socket::MultiplexWrite() { return true; }

ConnectionSocket::ConnectionSocket(int sock, int type, int protocol) : Socket(sock, type, protocol) {}
ConnectionSocket::~ConnectionSocket() {}
void ConnectionSocket::OnError(const std::string &) {}

bool ConnectionSocket::MultiplexEvent()
{
	if (this->HasFlag(SS_CONNECTED))
		return true;
	else if (this->HasFlag(SS_CONNECTING))
	{
		int		  optval = 0;
		socklen_t optlen = sizeof(int);
		if (!getsockopt(this->sock_fd, SOL_SOCKET, SO_ERROR, reinterpret_cast<char *>(&optval), &optlen) && !optval)
		{
			this->RemoveFlag(SS_CONNECTING);
			this->SetFlag(SS_CONNECTED);
			this->OnConnect();
		}
		else
		{
			errno = optval;
			this->OnError(optval ? strerror(errno) : "");
			wlog("Socket {} being killed before connect() could finish.", this->sock_fd);
			this->SetFlag(SS_DEAD);
		}
	}
	else
		this->SetFlag(SS_DEAD);

	return false;
}

void ConnectionSocket::MultiplexError()
{
	int		  optval = 0;
	socklen_t optlen = sizeof(int);
	getsockopt(this->sock_fd, SOL_SOCKET, SO_ERROR, reinterpret_cast<char *>(&optval), &optlen);
	errno = optval;
	this->OnError(optval ? strerror(errno) : "");
}

void ConnectionSocket::Connect(const std::string &conaddr, in_port_t p)
{
	// Set some variables.
	this->address = conaddr;
	this->port	= p;

	// Get our address family.
	this->sa.sa.sa_family = (conaddr.find(":") != std::string::npos ? AF_INET6 : AF_INET);
	// Set our port
	*(this->sa.sa.sa_family == AF_INET ? &this->sa.v4.sin_port : &this->sa.v6.sin6_port) = static_cast<in_port_t>(htons(p));

	// Convert our address to a sockaddr_t structure.
	int value = -1;
	if (this->sa.sa.sa_family == AF_INET)
		value = inet_pton(this->sa.sa.sa_family, conaddr.c_str(), &this->sa.v4.sin_addr);
	else if (this->sa.sa.sa_family == AF_INET6)
		value = inet_pton(this->sa.sa.sa_family, conaddr.c_str(), &this->sa.v6.sin6_addr);

	switch (value)
	{
		case 1: // success
			break;
		case 0:
			return this->SetError(EINVAL, "Invalid Host");
		default:
			return this->SetError(errno);
			// throw SocketException("Invalid host: %s", strerror(errno));
	}

	SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();

	if (::connect(this->sock_fd, &this->sa.sa, sizeof(struct sockaddr)) == -1)
	{
		if (errno != EINPROGRESS)
			this->OnError(strerror(errno));
		else
		{
			this->SetFlags(SS_CONNECTING, MX_WRITABLE);
			mplexer->UpdateSocket(this);
		}
	}
	else
	{
		this->SetFlag(SS_CONNECTED);
		this->OnConnect();
	}
}

// Set the socket to fd 0, which is a valid socket fd but 
// in our specific case, we want to initalize our own fds.
Pipe::Pipe() : Socket(0), write_pipe(-1)
{
	int fds[2];
	if (pipe(fds))
	{
		this->SetError(errno);
		return;
	}

	int sflags = fcntl(fds[0], F_GETFL, 0);
	fcntl(fds[0], F_SETFL, sflags | O_NONBLOCK);
	sflags = fcntl(fds[1], F_GETFL, 0);
	fcntl(fds[1], F_SETFL, sflags | O_NONBLOCK);

	this->sock_fd = fds[0];
	this->write_pipe = fds[1];

	SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();
	// Add the socket to the multiplexer since we specified a 0 fd
	// in the constructor
	mplexer->AddSocket(this);
	// Tell the multiplexer we need to read.
	this->SetFlag(MX_READABLE);
	mplexer->UpdateSocket(this);
}

Pipe::~Pipe()
{
	if (this->write_pipe >= 0)
		::close(this->write_pipe);
}

ssize_t Pipe::Write(const void *data, size_t sz)
{
	return write(this->write_pipe, data, sz);
}

ssize_t Pipe::Read(void *data, size_t sz)
{
	return read(this->GetFD(), data, sz);
}

bool Pipe::SetWriteBlocking(bool state)
{
	int f = fcntl(this->write_pipe, F_GETFL, 0);
	if (state)
		return !fcntl(this->write_pipe, F_SETFL, f & ~O_NONBLOCK);
	else
		return !fcntl(this->write_pipe, F_SETFL, f | O_NONBLOCK);
}

SecureConnectionSocket::SecureConnectionSocket(int sock, int type, int protocol)
	 : Socket(sock, type, protocol), ctx(nullptr), ssl(nullptr)
{
	SSL_load_error_strings();
	SSL_library_init();
	SSLeay_add_ssl_algorithms();

	const SSL_METHOD *method = TLS_client_method();
	this->ctx				 = SSL_CTX_new(method);
	if (ctx == nullptr)
	{
		wlog("[Socket][SSL] Cannot create SSL context");
		this->SetFlag(SS_DEAD);
	}
}

SecureConnectionSocket::~SecureConnectionSocket()
{
	SSL_shutdown(this->ssl);
	SSL_free(this->ssl); 
	SSL_CTX_free(this->ctx);
	ERR_free_strings();
	EVP_cleanup();
}

void SecureConnectionSocket::OnError(const std::string &) {}

bool SecureConnectionSocket::MultiplexEvent()
{
	if (this->HasFlag(SS_CONNECTED))
	{
		if (this->HasFlag(SS_SSL_HANDSHAKING))
			this->InitializeSSL();
		return true;
	}
	else if (this->HasFlag(SS_CONNECTING))
	{
		int		  optval = 0;
		socklen_t optlen = sizeof(int);
		if (!getsockopt(this->sock_fd, SOL_SOCKET, SO_ERROR, reinterpret_cast<char *>(&optval), &optlen) && !optval)
		{
			this->RemoveFlag(SS_CONNECTING);
			this->SetFlag(SS_CONNECTED);
			this->InitializeSSL();
			return true;
		}
		else
		{
			errno = optval;
			this->OnError(optval ? strerror(errno) : "");
			ilog("Socket {} being killed before connect() could finish.", this->sock_fd);
			this->SetFlag(SS_DEAD);
		}
	}
	else
		this->SetFlag(SS_DEAD);

	return false;
}

void SecureConnectionSocket::MultiplexError()
{
	int		  optval = 0;
	socklen_t optlen = sizeof(int);
	getsockopt(this->sock_fd, SOL_SOCKET, SO_ERROR, reinterpret_cast<char *>(&optval), &optlen);
	errno = optval;
	this->OnError(optval ? strerror(errno) : "");
}

ssize_t SecureConnectionSocket::Write(const void *data, size_t len)
{
	int olen = SSL_write(this->ssl, data, static_cast<int>(len));
	if (olen < 0)
	{
		int err = SSL_get_error(this->ssl, olen);
		switch (err)
		{
			case SSL_ERROR_WANT_WRITE:
			case SSL_ERROR_WANT_READ:
				return 0;
			case SSL_ERROR_ZERO_RETURN:
			case SSL_ERROR_SYSCALL:
			case SSL_ERROR_SSL:
			default:
				// Socket *s = static_cast<Socket *>(this);
				// s->SetFlag(SS_DEAD);
				this->SetError(err, this->get_ssl_errors());
		}
	}
	return static_cast<ssize_t>(olen);
}

ssize_t SecureConnectionSocket::Read(void *data, size_t len)
{
	int ilen = SSL_read(this->ssl, data, static_cast<int>(len));
	if (ilen < 0)
	{
		int err = SSL_get_error(this->ssl, ilen);
		switch (err)
		{
			case SSL_ERROR_WANT_WRITE:
			case SSL_ERROR_WANT_READ:
				return 0;
			case SSL_ERROR_ZERO_RETURN:
			case SSL_ERROR_SYSCALL:
			case SSL_ERROR_SSL:
			default:
				// this->SetFlag(SS_DEAD);
				this->SetError(err, this->get_ssl_errors());
		}
	}
	return static_cast<ssize_t>(ilen);
}

void SecureConnectionSocket::Connect(const std::string &conaddr, in_port_t theport)
{
	// Set some variables.
	this->address = conaddr;
	this->port	  = theport;

	// Get our address family.
	this->sa.sa.sa_family = (conaddr.find(":") != std::string::npos ? AF_INET6 : AF_INET);
	// Set our port
	*(this->sa.sa.sa_family == AF_INET ? &this->sa.v4.sin_port : &this->sa.v6.sin6_port) = static_cast<in_port_t>(htons(theport));

	// Convert our address to a sockaddr_t structure.
	int value = -1;
	if (this->sa.sa.sa_family == AF_INET)
		value = inet_pton(this->sa.sa.sa_family, conaddr.c_str(), &this->sa.v4.sin_addr);
	else if (this->sa.sa.sa_family == AF_INET6)
		value = inet_pton(this->sa.sa.sa_family, conaddr.c_str(), &this->sa.v6.sin6_addr);

	switch (value)
	{
		case 1: // success
			break;
		case 0:
			return this->SetError(EINVAL, "Invalid Host");
		default:
			return this->SetError(errno);
			// throw SocketException("Invalid host: %s", strerror(errno));
	}

	SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();

	if (::connect(this->sock_fd, &this->sa.sa, sizeof(struct sockaddr)) == -1)
	{
		if (errno != EINPROGRESS)
			this->OnError(strerror(errno));
		else
		{
			this->SetFlags(SS_CONNECTING, MX_WRITABLE);
			mplexer->UpdateSocket(this);
		}
	}
	else
	{
		this->SetFlag(SS_CONNECTED);
		this->InitializeSSL();
	}
}

void SecureConnectionSocket::InitializeSSL()
{
	SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();
	if (!this->ssl)
	{
		this->ssl = SSL_new(this->ctx);

		SSL_set_fd(this->ssl, this->GetFD());
		// we're a client.
		SSL_set_connect_state(this->ssl);
	}

	ERR_clear_error();

	if (int err = SSL_do_handshake(this->ssl); err < 0)
	{
		err = SSL_get_error(ssl, err);
		if (!this->HasFlag(SS_SSL_HANDSHAKING))
			this->SetFlag(SS_SSL_HANDSHAKING);
		switch (err)
		{
			case SSL_ERROR_WANT_READ:
				if (!this->HasFlag(MX_READABLE))
				{
					this->SetFlag(MX_READABLE);
					if (!mplexer->UpdateSocket(this))
						wlog("[Socket][SSL] Error setting socket as readable");
				}
				break;
			case SSL_ERROR_WANT_WRITE:
				if (!this->HasFlag(MX_WRITABLE))
				{
					this->SetFlags(MX_WRITABLE);
					if (mplexer->UpdateSocket(this))
						wlog("[Socket][SSL] Error setting socket as writable");
				}
				break;
			// case SSL_ERROR_WANT_X509_LOOKUP:
			// 	"it wants SSL_ERROR_WANT_X509_LOOKUP"_lw;
			// 	return;
			case SSL_ERROR_ZERO_RETURN:
				ilog("[Socket][SSL] Peer requested socket closure.");
				this->SetFlag(SS_DEAD);
				break;
			default:
				wlog("[Socket][SSL] Unknown SSL error: {} ({})", ERR_error_string(static_cast<unsigned long>(err), 0), err);
				this->SetFlag(SS_DEAD);
				break;
		}
		return;
	}

	// Our handshake is done, remove the flag.
	this->RemoveFlag(SS_SSL_HANDSHAKING);

	// Get some SSL info
	X509 *cert = SSL_get_peer_certificate(this->ssl);
	if (cert)
	{
		char *subject = X509_NAME_oneline(X509_get_subject_name(cert), nullptr, 0);
		char *issuer  = X509_NAME_oneline(X509_get_issuer_name(cert), nullptr, 0);

		ilog("[Socket][SSL] Subject: {}", subject);
		ilog("[Socket][SSL] Issuer: {}", issuer);
		delete subject;
		delete issuer;
		X509_free(cert);
	}

	ilog("[Socket][SSL] Successfully connected to TLS socket");
	// Call OnConnect
	this->OnConnect();
}


ListeningSocket::ListeningSocket(const std::string &bindaddr, in_port_t p, int domain) : Socket(-1)
{
	constexpr int op = 1;
	if (setsockopt(this->sock_fd, SOL_SOCKET, SO_REUSEADDR, &op, sizeof(op)) < 0)
		wlog("[Socket] Failed to reuse the bind address: {}", strerror(errno));

	// Set some variables.
	this->address = bindaddr;
	this->port	= p;

	// TODO: Is this buggy??
	// Set our address family 
	this->sa.sa.sa_family = static_cast<sa_family_t>(domain);

	// Set our port
	if (this->sa.sa.sa_family == AF_INET || this->sa.sa.sa_family == AF_INET6)
		*(this->sa.sa.sa_family == AF_INET ? &this->sa.v4.sin_port : &this->sa.v6.sin6_port) = static_cast<in_port_t>(htons(p));

	// Convert our address to a sockaddr_t structure.
	int value = -1;
	if (this->sa.sa.sa_family == AF_INET)
		value = inet_pton(this->sa.sa.sa_family, bindaddr.c_str(), &this->sa.v4.sin_addr);
	else if (this->sa.sa.sa_family == AF_INET6)
		value = inet_pton(this->sa.sa.sa_family, bindaddr.c_str(), &this->sa.v6.sin6_addr);
	else if (this->sa.sa.sa_family == AF_UNIX)
	{
		strcpy(this->sa.unx.sun_path, bindaddr.c_str());
		value = 1;
	}

	switch (value)
	{
		case 1: // success
			break;
		case 0:
			this->SetError(EINVAL, "Invalid bind address");
			return;
		default:
			this->SetError(errno);
			return;
			// throw SocketException("Invalid host: %s", strerror(errno));
	}

	if (::bind(this->sock_fd, &this->sa.sa, sizeof(this->sa.sa)) == -1)
	{
		elog("Failed to bind to {}:{}: {}", bindaddr, port, strerror(errno));
		this->SetError(errno);
		return;
	}

	if (::listen(this->sock_fd, SOMAXCONN) == -1)
	{
		elog("Failed to listen on {}:{}: {}", bindaddr, port, strerror(errno));
		this->SetError(errno);
		return;
	}
}

ListeningSocket::~ListeningSocket() {}

bool ListeningSocket::MultiplexRead()
{
	SocketAddress addr;

	socklen_t size	= sizeof(addr.sa);
	// https://rigtorp.se/sockets/
	int newsock = accept4(this->sock_fd, &addr.sa, &size, SOCK_NONBLOCK | SOCK_CLOEXEC);

	if (newsock >= 0)
	{
		ClientSocket *cs = this->OnAccept(newsock, std::move(addr));
		if (cs)
		{
			cs->SetFlag(SS_ACCEPTED);
			cs->OnAccept();;
		}
		else // This returns true so we're dependent on making sure things happen properly somewhere.
			this->SetError(EFAULT, "ListenSocket::OnAccept() returned nullptr for client socket");
	}
	else
		wlog("Unable to accept connection: {}", strerror(errno));

	return true;
}

ClientSocket::ClientSocket(ListeningSocket *listensocket, int fd, SocketAddress &&addr) : Socket(fd, addr.sa.sa_family), ls(listensocket)
{
	this->SetFlag(SS_ACCEPTING);
	this->sa = std::move(addr);
}

bool ClientSocket::MultiplexEvent()
{
	if (this->HasFlag(SS_ACCEPTED))
		return true;
	else if (this->HasFlag(SS_ACCEPTING))
	{
		this->SetFlag(SS_ACCEPTED);
		this->RemoveFlag(SS_ACCEPTING);
	}
	else
		this->SetFlag(SS_DEAD);

	return false;
}

void ClientSocket::MultiplexError()
{
	int		  optval = 0;
	socklen_t optlen = sizeof(int);
	getsockopt(this->sock_fd, SOL_SOCKET, SO_ERROR, reinterpret_cast<char *>(&optval), &optlen);
	errno = optval;
	this->OnError(optval ? strerror(errno) : "");
}

void ClientSocket::OnAccept() {}
void ClientSocket::OnError(const std::string &str) {}
