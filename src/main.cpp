/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <string>
#include <string_view>
#include <unistd.h>
#include <git2.h>
#include "Paths.h"
#include "Clone.h"
#include "SocketMultiplexer.h"
#include "Process.h"
#include "simplelog.h"
#include "FaultHandler.h"
#include "Configuration.h"
#include "vendor/CLI11.hpp"
#include "vendor/toml.hpp"

namespace fs = std::filesystem;

/**
 * The concept with this is that we need to do a few things:
 *
 * 1. Read a config file specified on the command line
 * 2. Parse the config file to know where we're working
 * 3-1: if the repo is cloned, check upstream and verify it's not out of date.
 * 3-2: if this is in a first-run state, clone the repo.
 * 4-1: if this is the first-run state, run the runonce section of the config.
 * 4-2: if this is not the first-run, run the startup section of the config.
 * 5: Run the heartbeat section to verify the service is functional
 * 6: periodically check for updated branches, re-run the runonce section if so
 */

constexpr std::string_view fox = 
R"(                                                                   ,-,
                                                             _.-=;~ /_
                                                          _-~   '     ;.
                                                      _.-~     '   .-~-~`-._
                                                _.--~~:.             --.____88
                              ____.........--~~~. .' .  .        _..-------~~
                     _..--~~~~               .' .'             ,'
                 _.-~                        .       .     ` ,'
               .'                                    :.    ./
             .:     ,/          `                   ::.   ,'
           .:'     ,(            ;.                ::. ,-'
          .'     ./'.`.     . . /:::._______.... _/:.o/
         /     ./'. . .)  . _.,'               `88;?88|
       ,'  . .,/'._,-~ /_.o8P'                  88P ?8b
    _,'' . .,/',-~    d888P'                    88'  88|
 _.'~  . .,:oP'        ?88b              _..--- 88.--'8b.--..__
:     ...' 88o __,------.88o ...__..._.=~- .    `~~   `~~      ~-._
`.;;;:='    ~~            ~~~                ~-    -       -   -)";

int main(int argc, char **argv)
{	
	fmt::print("{}\n", fmt::styled(fox, fmt::fg(fmt::color::orange)));
	ilog("Лиса (fox) - v1.0.0");

	// Start with registering our signal handler for segfaults.
	InitSignals();

	// Parse the command line arguments
	CLI::App lisa{"Лиса (fox) - v1.0.0"};
	argv = lisa.ensure_utf8(argv);

	fs::path configfile{"lisa.toml"};
	// TODO: search more paths for the file. supervisord searches the following 
	// - ../etc/supervisord.conf (relative to executable)
	// - ../supervisord.conf (relative to executable)
	// - $CWD/supervisord.conf 
	// - $CWD/etc/supervisord.conf 
	// - /etc/supervisord.conf 
	// - /etc/supervisord/supervisord.conf 
	// and we should search similar paths.
	lisa.add_option("-c,--config", configfile, "Configuration file");

	CLI11_PARSE(lisa, argc, argv);

	ilog("Lisa PID is {}", getpid());

	// Parse the config.
	Configuration::ParseConfig(configfile);	

	auto &config = Configuration::config;

	// Change directories for the entire application
	{
		auto &working_directory = config["Deployment"]["working_dir"].ref<std::string>();
		ilog("Changing directory to {}", working_directory);
		if (!SetWorkingDirectory(working_directory))
			return EXIT_FAILURE;
	}


	// Bring up the socket multiplexer.
	SocketMultiplexer *mplexer = SocketMultiplexer::GetMultiplexer();
	mplexer->Initialize();

	// Now check if we've got internet.
	ilog("Checking for internet connectivity");


	const toml::node_view<toml::node> &repo = config["Repository"];

	ilog("Cloning {} on branch \"{}\"", repo["url"].value<std::string>(), repo["branch"].value<std::string>());

	// Start by making directories and cloning the repo.
	git_libgit2_init();

	auto repo_opt = CloneRepository(repo);

	// Now move onto checking 


	git_libgit2_shutdown();
	mplexer->Terminate();

	return EXIT_SUCCESS;
}
