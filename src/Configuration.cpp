/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Configuration.h"
#include "simplelog.h"
#include <unistd.h>

namespace fs = std::filesystem;
toml::parse_result Configuration::config;

void Configuration::ParseConfig(std::filesystem::path &path)
{
	// Verify the file exists.
	if (!std::filesystem::exists(path))
	{
		// Convert to a path.
		clog("Config file {} does not exist.", path.string());
		clog(" `- Current directory: {}", fs::current_path().string());
		_exit(EXIT_FAILURE);
	}

	// Parse the config.
	try 
	{
		Configuration::config = toml::parse_file(path.string());

#ifdef _DEBUG
		// 🤮
		std::stringstream ss;
		ss << toml::json_formatter(Configuration::config);
		// ilog("Config: {}", ss.str());
#endif

		// Validate the config sections exist.
		// TODO!
	}
	catch (const toml::parse_error &e)
	{
		clog("Parsing config failed: {}", e.what());
		clog("Exiting.");
		_exit(EXIT_FAILURE);
	}
	catch (const std::exception &ex)
	{
		clog("Parsing config failed: {}", ex.what());
		clog("Exiting.");
		_exit(EXIT_FAILURE);
	}
	catch (...)
	{
		clog("Caught unknown exception?");
		_exit(EXIT_FAILURE);
	}

}
