/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "SocketMultiplexer.h"
#include "simplelog.h"
#include <sys/epoll.h>
#include <unistd.h>

// Define the variable.
SocketMultiplexer *SocketMultiplexer::mplex_;

/**
 * This file is entirely a stub to make sure that the application links correctly.
 * These are actually implemented in the socket engine modules.
 */
// Well.. I had to give the base class *SOMETHING* as an argument
// or the compiler whines about the implicit deletion of the
// default constructor of the Provider class.
SocketMultiplexer::SocketMultiplexer() : epollhandle(-1)
{
}

SocketMultiplexer *SocketMultiplexer::GetMultiplexer()
{
	if (!SocketMultiplexer::mplex_)
		SocketMultiplexer::mplex_ = new SocketMultiplexer();

	return SocketMultiplexer::mplex_;
}

Socket *SocketMultiplexer::FindSocket(int sock_fd)
{
	for (auto it : this->Sockets)
	{
		if (it->GetFD() == sock_fd)
			return it;
	}
	return nullptr;
}

SocketMultiplexer::~SocketMultiplexer() {}

// Initalizers
int SocketMultiplexer::Initialize() 
{
	this->epollhandle = epoll_create1(EPOLL_CLOEXEC);

	if (this->epollhandle == -1)
		return errno;

	// Allocate some space
	Events.resize(10);

	return 0;
}

void SocketMultiplexer::Terminate() 
{
	// Iterate the sockets and close them.
	for (auto && it : this->Sockets)
		delete it;
	
	this->Sockets.clear();
	close(this->epollhandle);
}

// Sockets interact with these functions.
bool SocketMultiplexer::AddSocket(Socket *s) 
{ 
	epoll_t ev;
	memset(&ev, 0, sizeof(epoll_t));

	ev.events  = EPOLLIN | EPOLLERR | EPOLLET;
	ev.data.ptr = s;
	// ev.data.fd = s->GetFD();

	// Add to the socket thing.
	this->Sockets.emplace_back(s);

	if (epoll_ctl(this->epollhandle, EPOLL_CTL_ADD, s->GetFD(), &ev) == -1)
		return false;
	return true;
}

bool SocketMultiplexer::RemoveSocket(Socket *s) 
{
	epoll_t ev;
	memset(&ev, 0, sizeof(epoll_t));
	ev.data.ptr = s;
	// ev.data.fd   = s->GetFD();

	auto it = std::find(this->Sockets.begin(), this->Sockets.end(), s);
	if (it != this->Sockets.end())
		this->Sockets.erase(it);
	else
		wlog("Could not find {} in known sockets!", s->GetFD());

	if (epoll_ctl(this->epollhandle, EPOLL_CTL_DEL, s->GetFD(), &ev))
		return false;
	return true;
}

bool SocketMultiplexer::UpdateSocket(Socket *s) 
{ 
	// NOTE: Do not lock a mutex in this. Threads use this to mark sockets as
	// read to write, if we lock a mutex then there will be gridlock in
	// the application over when data is processed.
	epoll_t ev;
	memset(&ev, 0, sizeof(epoll_t));

	ev.events  = (s->HasFlag(MX_READABLE) ? EPOLLIN : 0) | (s->HasFlag(MX_WRITABLE) ? EPOLLOUT : 0);
	ev.data.ptr = s;
	// ev.data.fd = s->GetFD();

	if (epoll_ctl(this->epollhandle, EPOLL_CTL_MOD, s->GetFD(), &ev) == -1)
		return false;
	return true;
}

// This is called in the event loop to slow the program down and process sockets.
void SocketMultiplexer::Multiplex(time_t sleep) 
{
	errno = 0;
	int total = 0;
	// Make sure we have room for epoll events.
	if (this->Sockets.size() > Events.size())
		Events.resize(this->Events.size() * 2);

	// Check for epoll events, wait for 1 second, then return.
	total = epoll_wait(this->epollhandle, &Events.front(), Events.size(), sleep * 1000);

	if (total == -1)
	{
		// elog("[Socket Engine] Multiplexer error: {}", strerror(errno));
		if (errno != EINTR)
		{
			elog("[Socket Engine] EPollMultiplexer::Process() error: {}", strerror(errno));
			::sleep(sleep); // prevents infinite loops my terminal has a hard time exiting from .
		}
	}

	for (int i = 0; i < total; ++i)
	{
		epoll_t &ev = Events[i];

		// Socket *s  = this->FindSocket(ev.data.fd);
		Socket *s = reinterpret_cast<Socket*>(ev.data.ptr);
		if (!s)
		{
			wlog("[Socket Engine] Unknown socket, ignoring...");
			// epoll_ctl(this->epollhandle, EPOLL_CTL_DEL, ev.data.fd, &ev);
			continue;
		}

		if (ev.events & (EPOLLHUP | EPOLLERR))
		{
			s->MultiplexError();
			delete s;
			continue;
		}

		if (!s->MultiplexEvent())
		{
			if (s->HasFlag(SS_DEAD))
				delete s;
			continue;
		}

		if ((ev.events & EPOLLIN) && !s->MultiplexRead())
			s->SetFlag(SS_DEAD);

		if ((ev.events & EPOLLOUT) && !s->MultiplexWrite())
			s->SetFlag(SS_DEAD);

		if (s->HasFlag(SS_DEAD))
			delete s;
	}
}
