/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <link.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <string>
#include <string_view>
#include <vector>
#include <sys/utsname.h>
#include <signal.h>
#include "simplelog.h"

#define SYSTEM_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))

static const std::string_view b64map{"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="};


std::string base64_encode(const std::string& data) 
{
    std::string encoded;
    encoded.reserve(((data.size() + 2) / 3) * 4);

    for (size_t i = 0; i < data.size(); i += 3) 
	{
        uint32_t word = (data[i] << 16) | ((i + 1 < data.size() ? data[i + 1] : 0) << 8) | (i + 2 < data.size() ? data[i + 2] : 0);
        encoded.push_back(b64map[(word >> 18) & 0x3f]);
        encoded.push_back(b64map[(word >> 12) & 0x3f]);
        encoded.push_back(i + 1 < data.size() ? b64map[(word >> 6) & 0x3f] : '=');
        encoded.push_back(i + 2 < data.size() ? b64map[word & 0x3f] : '=');
    }

    return encoded;
}

std::optional<std::string> base64_decode(const std::string& encoded) 
{
    std::string data;
    data.reserve((encoded.size() / 4) * 3);

    uint32_t word = 0;
    size_t word_bits = 0;

    for (char c : encoded) 
	{
        if (c == '=')
            break;

        size_t index = b64map.find(c);
        if (index == std::string_view::npos)
            // Invalid character
            return std::nullopt;

        word = (word << 6) | index;
        word_bits += 6;

        if (word_bits >= 8)
		{
            data.push_back((word >> (word_bits - 8)) & 0xff);
            word_bits -= 8;
        }
    }

    if (word_bits >= 6)
        // Invalid padding
        return std::nullopt;

    return data;
}


// Pack si_signo and si_code into a single 32-bit integer
constexpr uint32_t pack_signal_info(int signo, int code) 
{
    uint32_t packed = (signo << 16) | (code & 0xffff);
    return packed;
}

// Unpack the si_signo and si_code from a packed signal info value
constexpr void unpack_signal_info(uint32_t packed, int& signo, int& code) 
{
    signo = packed >> 16;
    code = packed & 0xffff;
}

// this will convert a character to an integer from the above array
// while avoiding an O(n) operation, branch prediction is usually
// faster but it could be argued that the O(n) would be the same.
constexpr int8_t char_to_int(char ch)
{
    if (ch >= 'A' && ch <= 'Z')
        return ch - 'A';
    else if (ch >= 'a' && ch <= 'z')
        return ch - 'a' + 26;
    else if (ch >= '0' && ch <= '9')
        return ch - '0' + 52;
    else if (ch == '+')
        return 62;
    else if (ch == '/')
        return 63;
    else if (ch == '=')
        return 64;
    else
        return -1;
}

std::string encode(std::vector<intptr_t> &&ints)
{
    std::string value;
    for (size_t i = 0; i < ints.size(); ++i)
    {
        intptr_t number = ints[i];

		if (number == 0)
			continue;

		if (number < 0)
			number = (-number << 1) | 1;
		else
			number <<= 1;

		do 
		{
			uint8_t clamped = number & 31;
			number >>= 5;

			if (number > 0)
				clamped |= 32;

			value += b64map[clamped];
		} while (number > 0);
    }

    return value;
}

std::vector<intptr_t> decode(std::string str)
{
	std::vector<intptr_t> addresses;

    int shift{0};
    intptr_t value{0};

    for (char ch : str)
    {
        int8_t integer = char_to_int(ch);
		// TODO: don't soft-fail?
        if (integer < 0)
            continue;

        // fmt::print("decoding {} as {}\n", ch, integer);
        bool has_continuation_bit = integer & 32;
        integer &= 31;
        value += integer << shift;

        if (has_continuation_bit)
            shift += 5;
        else
        {
            bool should_negate = value & 1;
            value >>= 1;
            if (should_negate)
                addresses.emplace_back(value == 0 ? -0x80000000 : -value);
            else
                addresses.emplace_back(value);

            value = shift = 0;
        }
    }

    return addresses;
}

// This is an attempt to replicate a similar system as described in bun's code:
// https://bun.sh/blog/bun-report-is-buns-new-crash-reporter

// A struct to contain all the PT_LOAD address structures
struct AddressContext 
{
	// Address we're interested in.
	intptr_t address;

	// Module's base address that was found.
	intptr_t mod_baseaddr;

	// Base address that is calculated.
	intptr_t relative;

	// Whether the base was found or not.
	bool found;
};

// Array of pointers for stack frames
void *array[1024];

AddressContext FindRelativeAddress(intptr_t Address)
{
	AddressContext ctx_orig{
		.address = Address,
		.mod_baseaddr = 0,
		.relative = 0,
		.found = false
	};

	int ret = dl_iterate_phdr([](struct dl_phdr_info *info, size_t size, void *data) -> int
	{
		AddressContext &context = *reinterpret_cast<AddressContext*>(data);
		// Just exit this call.
		if (context.address < info->dlpi_addr) 
			return 0;

		// Iterate the program headers.
		for (size_t j = 0; j < info->dlpi_phnum; j++) 
		{
			const ElfW(Phdr) &phdr = info->dlpi_phdr[j];

			if (phdr.p_type != PT_LOAD)
				continue;

			// Find what module this address is in and verify it's in the 
			// address range, otherwise we don't care about it.
			const ElfW(Addr) seg_start = info->dlpi_addr + phdr.p_vaddr;
			const ElfW(Addr) seg_end = seg_start + phdr.p_memsz;
			if (context.address >= seg_start && context.address < seg_end)
			{
				context.mod_baseaddr = info->dlpi_addr;
				context.relative = context.address - info->dlpi_addr;
				// We succeeded in finding and address
				return 1;
			}
		}
		// Failed to find an address.
		return 0;

		// This is a C function, it cannot take a lambda object as a function pointer 
		// so we're forced to pass the context in as a pointer.
	}, reinterpret_cast<void*>(&ctx_orig));

	// Found a base address, return.
	if (ret)
		ctx_orig.found = true;

	return ctx_orig;
}


std::string bt()
{
	size_t size = backtrace(array, 1024);

	// printf("Showing %zu stack frames:\n", size);
	std::vector<intptr_t> addresses;

	// Print the pointers?
	for (size_t i = 0; i < size; ++i)
	{
		auto info = FindRelativeAddress(reinterpret_cast<intptr_t>(array[i]));
		addresses.emplace_back(info.relative);
		// printf("[%zu]: 0x%lx\n", i, info.relative);
	}

	std::string stackstr = encode(std::move(addresses));
	// printf("Stacktrace Str: %s\n", stackstr.c_str());
	return stackstr;

}

void printsiginf(siginfo_t *info)
{
	switch (info->si_signo)
	{
		case SIGILL:
		{
			switch (info->si_code)
			{
				case ILL_ILLOPC: clog("[SIGILL] Illegal Opcode."); break;
				case ILL_ILLOPN: clog("[SIGILL] Illegal operand."); break;
				case ILL_ILLADR: clog("[SIGILL] Illegal addressing mode."); break;
				case ILL_ILLTRP: clog("[SIGILL] Illegal trap."); break;
				case ILL_PRVOPC: clog("[SIGILL] Privileged opcode."); break;
				case ILL_PRVREG: clog("[SIGILL] Privileged register."); break;
				case ILL_COPROC: clog("[SIGILL] Coprocessor error."); break;
				case ILL_BADSTK: clog("[SIGILL] Internal stack error."); break;
				default: clog("[SIGILL] Unknown subcode {}", info->si_code); break;
			}
			// auto rel = FindRelativeAddress(reinterpret_cast<intptr_t>(info->si_addr));
			// clog("Faulting instruction address: %p (rel? 0x%lx)", info->si_addr, rel.relative);
			break;
		}
		case SIGFPE:
		{
			switch (info->si_code)
			{
				case FPE_INTDIV: clog("[SIGFPE] Integer divide by zero."); break;
				case FPE_INTOVF: clog("[SIGFPE] Integer overflow."); break;
				case FPE_FLTDIV: clog("[SIGFPE] Floating-point divide by zero."); break;
				case FPE_FLTOVF: clog("[SIGFPE] Floating-point overflow."); break;
				case FPE_FLTUND: clog("[SIGFPE] Floating-point underflow."); break;
				case FPE_FLTRES: clog("[SIGFPE] Floating-point inexact result."); break;
				case FPE_FLTINV: clog("[SIGFPE] Floating-point invalid operation."); break;
				case FPE_FLTSUB: clog("[SIGFPE] Subscript out of range."); break;
				default: clog("[SIGFPE] Unknown subcode {}", info->si_code); break;
			}
			// auto rel = FindRelativeAddress(reinterpret_cast<intptr_t>(info->si_addr));
			// clog("Faulting instruction address: %p (rel? 0x%lx)", info->si_addr, rel.relative);
			break;
		}
		case SIGSEGV:
		{
			switch (info->si_code)
			{
				case SEGV_MAPERR: clog("[SIGSEGV] Address not mapped to object."); break;
				case SEGV_ACCERR: clog("[SIGSEGV] Invalid permissions for mapped object."); break;
				case SEGV_BNDERR : clog("[SIGSEGV] Failed address bound checks."); break;
				case SEGV_PKUERR : clog("[SIGSEGV] Access was denied by memory protection keys. See pkeys(7). The protection key which applied to this access is available via si_pkey."); break;
				default: clog("[SIGSEGV] Unknown subcode %d", info->si_code); break;
			}
			// auto rel = FindRelativeAddress(reinterpret_cast<intptr_t>(info->si_addr));
			// clog("Faulting instruction address: %p (rel? 0x%lx)", info->si_addr, rel.relative);
			break;
		}
		case SIGBUS:
		{
			switch (info->si_code)
			{
				case BUS_ADRALN: clog("[SIGBUS] Invalid address alignment."); break;
				case BUS_ADRERR: clog("[SIGBUS] Nonexistent physical address."); break;
				case BUS_OBJERR: clog("[SIGBUS] Object-specific hardware error."); break;
				case BUS_MCEERR_AR: clog("[SIGBUS] Hardware memory error consumed on a machine check; action required."); break;
				case BUS_MCEERR_AO: clog("[SIGBUS] Hardware memory error detected in process but not consumed; action optional."); break;
				default: clog("[SIGBUS] Unknown subcode %d", info->si_code); break;
			}
			break;
		}
		case SIGABRT:
		{
			auto rel = FindRelativeAddress(reinterpret_cast<intptr_t>(info->si_addr));
			clog("[SIGABRT] Abort at address: {} (rel? 0x{:x})", info->si_addr, rel.relative);
			break;
		}
		default:
			wlog("Unknown signal {} with subcode {}", info->si_signo, info->si_code);
	}
}

void sigact(int sig, siginfo_t *info, void *uctx)
{
	printsiginf(info);

	switch (sig)
	{
		case SIGHUP:
			// signal(sig, SIG_IGN);
			// Rehash();
			ilog("[Signal] Reloading config is not yet supported...");
			break;
		case SIGPIPE:
			// signal(sig, SIG_IGN);
			ilog("[Signal] Received SIGPIPE, ignoring...");
			break;
		case SIGABRT: [[fallthrough]];
		case SIGILL: [[fallthrough]];
		case SIGBUS: [[fallthrough]];
		case SIGFPE: [[fallthrough]];
		case SIGSEGV:
		{
			// get architecture info 
			struct utsname uts;
			uname(&uts);
			char buffer[4096]{0};

			snprintf(buffer, sizeof(buffer), "%s %s %s\n", uts.sysname, uts.release, uts.machine);

			// clog("[Signal] Fault signal");
			std::string stacktrace = bt();

			//(2.0.0-518cec2 x86_64 tdlib 1.8.26
			// TODO: Add our git hash
			stacktrace += "518cec2";

			// Pack the signal number, then encode it.
			uint32_t packed = pack_signal_info(info->si_signo, info->si_code);
			// Pack the version.
			uint32_t version = SYSTEM_VERSION(1, 0, 0);
			// cast them into "pointers" and append to array,

			std::vector<intptr_t> ptrs{{packed, version}};
			stacktrace += encode(std::move(ptrs));
			stacktrace += base64_encode(buffer);

			clog("[STACKTRACE] {}", stacktrace);

			// Oh no, core crash, report it and exit
			_exit(sig);
			break;
		}
		case SIGINT:
		case SIGTERM:
		{
			ilog("[Signal] Received SIGTERM/SIGINT, terminating...");
			break;
		}
		default:
			wlog("[Signal] Received weird signal from terminal: {}", sig);
	}
}

void InitSignals()
{
	struct sigaction act{};
	act.sa_handler = nullptr;
	act.sa_sigaction = sigact;
	// We want to get extra info from the signal.
	act.sa_flags |= SA_SIGINFO;
	// All the signals we want to catch
	sigemptyset(&act.sa_mask);

	// Macro to make shit cleaner
#define ADDSIG(sa, sig) sigaddset(&sa.sa_mask, sig); sigaction(sig, &sa, nullptr)
	ADDSIG(act, SIGINT);
	ADDSIG(act, SIGTERM);
	ADDSIG(act, SIGFPE);
	ADDSIG(act, SIGSEGV);
	ADDSIG(act, SIGPIPE);
	ADDSIG(act, SIGILL);
	ADDSIG(act, SIGBUS);
	ADDSIG(act, SIGHUP);
	ADDSIG(act, SIGABRT);
}
