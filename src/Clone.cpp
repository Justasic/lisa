/*
 * Copyright (c) 2024, Justin Crawford
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Clone.h"
#include "simplelog.h"
#include "Configuration.h"
#include <chrono>
#include <cmath>
#include <git2/clone.h>
#include <memory>
#include <string>
#include "vendor/indicators/progress_bar.hpp"
#include "vendor/indicators/dynamic_progress.hpp"

struct progress_info 
{
	git_indexer_progress fetch_progress;
	size_t completed_steps;
	size_t total_steps;
	// Used for caluclating speed.
	size_t prev_bytes;

	bool finished;
	const toml::node_view<toml::node> &table;
	const char *path;

	// 2 progress bars for receiving objects and indexing objects.
	// 1st bar is object fetch progress 
	// 2nd bar is object indexing progress.
	indicators::DynamicProgress<indicators::ProgressBar> bars;
	// Since the above bars have an awful API, we'll track whether 
	// we set the damn progress option here.
	bool set_values;
	// Track our speed by having the start time and current time
	std::chrono::time_point<std::chrono::steady_clock> start_time;
	std::chrono::time_point<std::chrono::steady_clock> current_time;

	// Track what bars are in what positions.
	size_t checkout_pos;
	size_t delta_pos;
};

template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
std::pair<const char*, T> GetHighestSize(T size)
{
	std::array<const char*, 9> sizes{"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"};
	size_t			   si	  = 0;
	for (; 1024 < size; si++)
		size >>= 10;

	if (si < sizes.size())
		return std::make_pair(sizes[si], std::move(size));
	else
		return std::make_pair("(hello future!)", std::move(size));
}

static void print_progress(progress_info *pd)
{
	// if we're still under a second, just return.
	if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - pd->current_time).count() == 0)
		return;

	pd->current_time = std::chrono::steady_clock::now();

	if (pd->fetch_progress.total_objects && pd->fetch_progress.received_objects == pd->fetch_progress.total_objects) 
	{
		if (!pd->delta_pos)
		{
			auto delta_bar = std::make_unique<indicators::ProgressBar>(
				indicators::option::BarWidth{50},
				indicators::option::Start{"["},
				indicators::option::Fill{"■"},
				indicators::option::Lead{"■"},
				indicators::option::Remainder{" "},
				indicators::option::End{" ]"},
				indicators::option::ForegroundColor{indicators::Color::grey},
				indicators::option::ShowElapsedTime{true},
				indicators::option::ShowRemainingTime{true},
				indicators::option::MaxProgress{pd->fetch_progress.total_deltas},
				indicators::option::PrefixText{"Resolving deltas "},
				indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}
			);
			pd->delta_pos = pd->bars.push_back(std::move(delta_bar));

			// Mark the other bars as completed.
			pd->bars[0].mark_as_completed();
			pd->bars[1].mark_as_completed();
		}
		pd->bars[pd->delta_pos].set_progress(pd->fetch_progress.indexed_deltas);
		pd->bars[pd->delta_pos].set_option(indicators::option::PostfixText{fmt::format("{}/{}", pd->fetch_progress.indexed_deltas, pd->fetch_progress.total_deltas)});

		// printf("Resolving deltas %u/%u\r", pd->fetch_progress.indexed_deltas, pd->fetch_progress.total_deltas);
	}
	else 
	{
		// Calculate download speed.
		double speed = 0.0;
		auto now = std::chrono::steady_clock::now();
		if (now != pd->start_time && pd->fetch_progress.received_bytes - pd->prev_bytes)
		{
			using namespace std::literals;
			auto delta = std::chrono::duration_cast<std::chrono::seconds>(now - pd->start_time);
			if (delta.count())
				speed = (pd->fetch_progress.received_bytes - pd->prev_bytes) / (delta.count());
		}

		auto speedpair = GetHighestSize(static_cast<size_t>(std::round(speed)));
		auto downloadtotal = GetHighestSize(pd->fetch_progress.received_bytes);

		// Set the maximum progress of the bars.
		if (!pd->set_values)
		{
			pd->bars[0].set_option(indicators::option::MaxProgress(pd->fetch_progress.total_objects));
			pd->bars[1].set_option(indicators::option::MaxProgress(pd->fetch_progress.total_objects));
			pd->set_values = true;
		}

		if (pd->total_steps || pd->completed_steps)
		{
			auto checkout_bar = std::make_unique<indicators::ProgressBar>(
				indicators::option::BarWidth{50},
				indicators::option::Start{"["},
				indicators::option::Fill{"■"},
				indicators::option::Lead{"■"},
				indicators::option::Remainder{" "},
				indicators::option::End{" ]"},
				indicators::option::ForegroundColor{indicators::Color::grey},
				indicators::option::ShowElapsedTime{true},
				indicators::option::ShowRemainingTime{true},
				indicators::option::MaxProgress{pd->total_steps},
				indicators::option::PrefixText{"Checking out     "},
				indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}
			);
			pd->checkout_pos = pd->bars.push_back(std::move(checkout_bar));
		}

		// Update the progress bars.
		pd->bars[0].set_progress(pd->fetch_progress.received_objects); // fetch progress.
		pd->bars[1].set_progress(pd->fetch_progress.indexed_objects);  // index progress.
		if (pd->checkout_pos)
			pd->bars[pd->checkout_pos].set_progress(pd->completed_steps);                 // checkout progress.

		// Update the fetch text.
		pd->bars[0].set_option(indicators::option::PostfixText{fmt::format("{}/{} {} {}/s ({} {} downloaded)", 
					pd->fetch_progress.received_objects, 
					pd->fetch_progress.total_objects, 
					speedpair.second, speedpair.first,
					downloadtotal.second, downloadtotal.first
		)});
		// Update the indexing text
		pd->bars[1].set_option(indicators::option::PostfixText{fmt::format("{}/{}", pd->fetch_progress.indexed_objects, pd->fetch_progress.total_objects)});
		// Update the checkout text
		if (pd->checkout_pos)
			pd->bars[pd->checkout_pos].set_option(indicators::option::PostfixText{fmt::format("{}/{} steps", pd->completed_steps, pd->total_steps)});
		// Update the previous bytes value
		pd->prev_bytes = pd->fetch_progress.received_bytes;

		// printf("net %3d%% (%4zu kb, %5u/%5u)  /  idx %3d%% (%5u/%5u)  /  chk %3d%% (%4zu/%4zu)%s\n",
		//    network_percent, kbytes,
		//    pd->fetch_progress.received_objects, pd->fetch_progress.total_objects,
		//    index_percent, pd->fetch_progress.indexed_objects, pd->fetch_progress.total_objects,
		//    checkout_percent,
		//    pd->completed_steps, pd->total_steps,
		//    pd->path);
	}
}

static int sideband_progress(const char *str, int len, void *payload)
{
	(void)payload; /* unused */

	std::string_view sv(str, len);
	icrlog("remote: {}", sv);
	return 0;
}

static int fetch_progress(const git_indexer_progress *stats, void *payload)
{
	progress_info *pi = reinterpret_cast<progress_info*>(payload);
	pi->fetch_progress = *stats;
	print_progress(pi);
	return 0;
}
static void checkout_progress(const char*path, size_t cur, size_t tot, void *payload)
{
	progress_info *pd = reinterpret_cast<progress_info*>(payload);
	pd->completed_steps = cur;
	pd->total_steps = tot;
	pd->path = path;
	print_progress(pd);
}

// Handle authentication! :D
int cred_acquire_cb(git_cred** out, const char* url, const char* username_from_url, unsigned int allowed_types, void* payload) 
{
	// We need this to get the config values for authentication if required.
	progress_info *pi = reinterpret_cast<progress_info*>(payload);

	// Start with a default user of "git" as most git servers use that user.
	std::string username{"git"};
	if (username_from_url)
		username = username_from_url;
	else if (pi->table["auth_username"].value<std::string>())
		username = pi->table["auth_username"].ref<std::string>();

	std::string ssh_passphrase;
	if (pi->table["ssh_passphrase"].value<std::string>())
		ssh_passphrase = pi->table["ssh_passphrase"].ref<std::string>();

    if (allowed_types & GIT_CREDTYPE_SSH_KEY)
	{
		// Check if they specified a keyfile or if they specified the key itself.
		if (pi->table["ssh_private_key_file"].value<std::string>() && pi->table["ssh_public_key_file"].value<std::string>())
		{
			const std::string &ssh_public_key_path = pi->table["ssh_public_key_file"].ref<std::string>();
			const std::string &ssh_private_key_path = pi->table["ssh_private_key_file"].ref<std::string>();

			return git_cred_ssh_key_new(out, username.c_str(), ssh_public_key_path.c_str(), ssh_private_key_path.c_str(), ssh_passphrase.c_str());
		}
		else if (pi->table["ssh_private_key"].value<std::string>() && pi->table["ssh_public_key"].value<std::string>())
		{
			const std::string &ssh_private_key = pi->table["ssh_private_key"].ref<std::string>();
			const std::string &ssh_public_key = pi->table["ssh_public_key"].ref<std::string>();

			return git_credential_ssh_key_memory_new(out, username.c_str(), ssh_public_key.c_str(), ssh_private_key.c_str(), ssh_passphrase.c_str());
		}
		else 
			elog("SSH clone requested a key file but the configuration does not satisfy the requirement, attempting to try a different option if available...");
	}

	if (allowed_types & GIT_CREDTYPE_USERPASS_PLAINTEXT)
	{
		if (pi->table["http_token"].value<std::string>())
		{
			const std::string &http_token = pi->table["http_token"].ref<std::string>();
			return git_cred_userpass_plaintext_new(out, "x-access-token", http_token.c_str());
		}
		else if (pi->table["auth_username"].value<std::string>() && pi->table["auth_password"].value<std::string>())
		{
			const std::string &auth_password = pi->table["auth_password"].ref<std::string>();
			return git_cred_userpass_plaintext_new(out, username.c_str(), auth_password.c_str());
		}
		elog("Clone requires an HTTP authentication but none specified, attempting to try a different option if available...");
	}
	
	if (allowed_types & GIT_CREDTYPE_USERNAME)
		return git_cred_username_new(out, username.c_str());

    return -1;
}

// TODO: use std::unique_ptr instead of raw ptrs.
// Have to use std::string and not string_view as we call .c_str()
std::optional<git_repo_ptr> CloneRepository(const toml::node_view<toml::node> &table)
{
	const std::string &repourl = table["url"].ref<std::string>();
	const std::string &clonepath = table["path"].ref<std::string>();
	const auto &branch = table["branch"].value<std::string>();

	// Setup progress bars.
	auto fetch_bar = std::make_unique<indicators::ProgressBar>(
		indicators::option::BarWidth{50},
		indicators::option::Start{"["},
		indicators::option::Fill{"■"},
		indicators::option::Lead{"■"},
		indicators::option::Remainder{" "},
		indicators::option::End{" ]"},
		indicators::option::ForegroundColor{indicators::Color::green},
		indicators::option::ShowElapsedTime{true},
		indicators::option::ShowRemainingTime{true},
		indicators::option::MaxProgress{1000},
		indicators::option::PrefixText{"Fetching Objects "},
		indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}
	);

	auto index_bar = std::make_unique<indicators::ProgressBar>(
		indicators::option::BarWidth{50},
		indicators::option::Start{"["},
		indicators::option::Fill{"■"},
		indicators::option::Lead{"■"},
		indicators::option::Remainder{" "},
		indicators::option::End{" ]"},
		indicators::option::ForegroundColor{indicators::Color::cyan},
		indicators::option::ShowElapsedTime{true},
		indicators::option::ShowRemainingTime{true},
		indicators::option::MaxProgress{1000},
		indicators::option::PrefixText{"Indexing Objects "},
		indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}
	);

	progress_info pi
	{
		.fetch_progress = {},
		.completed_steps = 0,
		.total_steps = 0,
		.prev_bytes = 0,
		.finished = false,
		.table = table,
		.path = nullptr,
		// Fucking retarded bullshit god damn I hate C++ sometimes.
		.bars = indicators::DynamicProgress<indicators::ProgressBar>(std::move(fetch_bar), std::move(index_bar)),
		.set_values = false,
		.start_time = std::chrono::steady_clock::now(),
		.current_time = pi.start_time,
		.checkout_pos = 0,
		.delta_pos = 0
	};

// Ignore annoying warnings we cannot fix.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
	git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
	git_checkout_options checkout_opts = GIT_CHECKOUT_OPTIONS_INIT;
#pragma GCC diagnostic pop

	// Setup options
	checkout_opts.checkout_strategy = GIT_CHECKOUT_SAFE;
	checkout_opts.progress_cb = checkout_progress;
	checkout_opts.progress_payload = reinterpret_cast<void*>(&pi);
	// Setup clone options
	clone_opts.checkout_opts = checkout_opts;
	if (branch)
		clone_opts.checkout_branch = branch.value().c_str();
	clone_opts.fetch_opts.callbacks.sideband_progress = sideband_progress;
	clone_opts.fetch_opts.callbacks.transfer_progress = fetch_progress;
	// Handle credentials.
	clone_opts.fetch_opts.callbacks.credentials = cred_acquire_cb;
	clone_opts.fetch_opts.callbacks.payload = reinterpret_cast<void*>(&pi);

	// Hide the cursor while cloning.
	indicators::show_console_cursor(false);
	git_repository *repo = nullptr;
	int error = git_clone(&repo, repourl.c_str(), clonepath.c_str(), &clone_opts);
	// Show the cursor again
	indicators::show_console_cursor(true);

	if (error != 0)
	{
		const git_error *e = git_error_last();
		clog("Error cloning {}: {}", repourl, e->message ? e->message : "Unknown???");
		return std::nullopt;
	}

	git_repo_ptr retptr(repo, git_repository_free);
	return retptr;
}
