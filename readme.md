## Lisa (Лиса) or Fox 🦊 - The Container Bootstrapper

![screenshot](docs/screenshot.jpg)

### Overview

Lisa is an application designed to handle the bootstrapping of Docker containers. It manages various tasks, including git repository updates, cloning, authentication, watchdog actions, and application functions like restarting or providing information.

### Features

- **Git Repository Management**: Lisa handles cloning and updating of git repositories.
- **Authentication Methods**: Supports SSH keys, access tokens, and username/password authentication for secure git repository cloning.
- **Network Connectivity Check**: Verifies the container's network connectivity via an HTTP request.
- **Service Management**: Manages services within the container.
- **Healthcheck/Watchdog Function**: Ensures the service(s) inside the container are healthy.

### Usage

To use Lisa, follow these steps:

1. Place a `deployment.toml` file in the `/etc/lisa.toml` directory of your Docker container. You can find an example `deployment.toml` file in the `docs` subdirectory of this repository.
2. Run the `lisa` executable optionally with the desired configuration location specified using the `-c` option.

### Build Instructions

To build Lisa, follow these steps:

1. Clone the repository and all submodules using:
   ```bash
   git clone --recursive https://gitlab.com/Justasic/lisa.git
   ```
2. Create a new directory for building the project (e.g., `build`) within the cloned repository.
3. Navigate to the build directory and run the following CMake commands to configure and build the project:
   ```bash
   mkdir build && cd build
   cmake ..
   make
   ```

### Configuration

Lisa uses a TOML configuration file (`lisa.toml`) for its settings. If you prefer to specify a custom configuration location, use the `-c` command-line argument when running `lisa`.

### Licensing

Lisa is released under the 3-clause BSD license.

### Supported Platforms

Lisa is designed to run as PID 1 in a Docker container and manage various tasks necessary for its operation.
